(document.body);

var surveyJSON = require('./survey.json');
console.log(surveyJSON)

var assign = require('object-assign');
var createConfig = require('./custom_config');
var createRenderer = require('./lib/createRenderer');
var createLoop = require('raf-loop');
var contrast = require('wcag-contrast');

var canvas = document.querySelector('#canvas');
var background = new window.Image();
var context = canvas.getContext('2d');

var loop = createLoop();
var seedContainer = document.querySelector('.seed-container');
var seedText = document.querySelector('.seed-text');

var isIOS = /(iPad|iPhone|iPod)/i.test(navigator.userAgent);


var Sortable = require('sortablejs')

if (isIOS) { // iOS bugs with full screen ...
  const fixScroll = () => {
    setTimeout(() => {
      window.scrollTo(0, 1);
    }, 500);
  };

  fixScroll();
  window.addEventListener('orientationchange', () => {
    fixScroll();
  }, false);
}

window.addEventListener('resize', resize);
document.body.style.margin = '0';
canvas.style.position = 'absolute';

var randomize = (survey, ev) => {
  console.log(survey);
  document.body.style.overflow = 'hidden';
  if (ev) ev.preventDefault();
  reload(createConfig(survey));
};
//randomize();
resize();

// const addEvents = (element) => {
//   element.addEventListener('mousedown', (ev) => {
//     if (ev.button === 0) {
//       randomize(ev);
//     }
//   });
//   element.addEventListener('touchstart', randomize);
// };

// const targets = [ document.querySelector('#fill'), canvas ];
// targets.forEach(t => addEvents(t));

function reload (config) {
  loop.removeAllListeners('tick');
  loop.stop();

  var opts = assign({
    backgroundImage: background,
    context: context
  }, config);

  var pixelRatio = typeof opts.pixelRatio === 'number' ? opts.pixelRatio : 1;
  canvas.width = opts.width * pixelRatio;
  canvas.height = opts.height * pixelRatio;

  document.body.style.background = opts.palette[0];
  seedContainer.style.color = getBestContrast(opts.palette[0], opts.palette.slice(1));
  // seedText.textContent = opts.seedName;

  background.onload = () => {
    var renderer = createRenderer(opts);

    if (opts.debugLuma) {
      renderer.debugLuma();
    } else {
      renderer.clear();
      var stepCount = 0;
      loop.on('tick', () => {
        renderer.step(opts.interval);
        stepCount++;
        if (!opts.endlessBrowser && stepCount > opts.steps) {
          loop.stop();
        }
      });
      loop.start();
    }
  };

  background.src = config.backgroundSrc;
}

function resize () {
  letterbox(canvas, [ window.innerWidth, window.innerHeight ]);
}

function getBestContrast (background, colors) {
  var bestContrastIdx = 0;
  var bestContrast = 0;
  colors.forEach((p, i) => {
    var ratio = contrast.hex(background, p);
    if (ratio > bestContrast) {
      bestContrast = ratio;
      bestContrastIdx = i;
    }
  });
  return colors[bestContrastIdx];
}

// resize and reposition canvas to form a letterbox view
function letterbox (element, parent) {
  var aspect = element.width / element.height;
  var pwidth = parent[0];
  var pheight = parent[1];

  var width = pwidth;
  var height = Math.round(width / aspect);
  var y = Math.floor(pheight - height) / 2;

  if (isIOS) { // Stupid iOS bug with full screen nav bars
    width += 1;
    height += 1;
  }

  element.style.top = y + 'px';
  element.style.width = width + 'px';
  element.style.height = height + 'px';
}


// adding in the stuff for the survey
    Survey.Survey.cssType = "bootstrap";
    Survey.JsonObject.metaData.addProperty("checkbox", {name: "renderAs", default: "standard", choices: ["standard", "sortablejs"]});


var widget = {
    name: "sortablejs",
    isFit : function(question) { return question["renderAs"] === 'sortablejs'; },
    htmlTemplate: `<div></div>`,
    afterRender: function(question, el) {
      var $el = $(el);
      var style = {border: "1px solid #1ab394", width:"100%", minHeight:"50px" }
      $el.append(`
        <div style="width:50%">
          <div class="result">
            <span>move items here</span>
          </div>
          <div class="source" style="margin-top:10px;">
          </div>
        </div>
      `);
      var $source = $el.find(".source").css(style);
      var $result = $el.find(".result").css(style);
      var $emptyText = $result.find("span");
      question.visibleChoices.forEach(function(choice) {
        $source.append(`<div data-value="` + choice.value +  `">
                               <div style="background-color:#1ab394;color:#fff;margin:5px;padding:10px;">` + choice.text + `</div>
                             </div>`);
      });
      
      Sortable.create($result[0], {
          animation: 150,
          group: {
            name: 'top3',
            pull: true,
            put: true
          },
          onSort: function (evt) {
            var result = [];
            if (evt.to.children.length === 1) {
                $emptyText.css({display: "inline-block"});
            } else {
                $emptyText.css({display: "none"});
                for (var i = 1; i < evt.to.children.length; i++) {
                    result.push(evt.to.children[i].dataset.value)
                }
            }
            //question.value = result;
          },
      });
      Sortable.create($source[0], {
          animation: 150,
          group: {
            name: 'top3',
            pull: true,
            put: true
          }
      });
    }
}

Survey.CustomWidgetCollection.Instance.addCustomWidget(widget);


var survey = new Survey.Model(surveyJSON);
    $("#surveyContainer").Survey({
        model:survey,
        onComplete:sendDataToServer
    });
    
  function sendDataToServer(survey) {
    var resultAsString = JSON.stringify(survey.data);
    $("#result").show();
    $("#surveyContainer").hide();
    randomize(survey.data);
  };

$("#return").click(function(){
  location.reload();
});