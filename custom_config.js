var seedRandom = require('seed-random');
var palettes = require('./lib/color-palettes.json');
var createRandomRange = require('./lib/random-range');
var answers = require('./answers.json');

module.exports = function (results, seed) {
  if (typeof seed === 'undefined') {
    seed = String(Math.floor(Math.random() * 1000000));
  }

  console.log('Seed thing:', seed);
  console.log("results: ", results);

  var randomFunc = seedRandom(seed);
  var random = createRandomRange(randomFunc);

  var maps = answers["boundaries"][results.boundaries]
            .map(function (p) {
                  return 'maps/' + p;
            });

  var mapSrc = maps[parseInt(results.role) - 1];


  return {
    // rendering options
    random: randomFunc,
    seedName: seed,
    pointilism: random(0, 0.1),
    noiseScalar: [ random(0.000001, 0.000001), random(0.0002, 0.004) ],
    globalAlpha: answers["relevant"][results.relevant],
    startArea: answers.emotions[results.emotions],
    maxRadius: 50,
    lineStyle: (results.aversion == 1)? 'round' : 'square',
    interval: random(0.001, 0.01),
    count: Math.floor(random(answers.time[results["time-units"]][0],
                        answers.time[results["time-units"]][1])),
    steps: Math.floor(random(answers.quantity[results.quantity][0],
                        answers.quantity[results.quantity][1])),
    endlessBrowser: false, // Whether to endlessly step in browser

    // background image that drives the algorithm
    debugLuma: false,
    backgroundScale: 1,
    backgorundFille: 'black',
    backgroundSrc: mapSrc,

    // browser/node options
    pixelRatio: 1,
    width: 1280 * 2,
    height: 720 * 2,
    palette: getPalette(),

    // node only options
    asVideoFrames: false,
    filename: 'render',
    outputDir: 'output'
  };

  function getPalette () {
    var paletteColors = palettes[choosePalette() % palettes.length];
    return paletteColors;
  }

  function arrayShuffle (arr) {
    var rand;
    var tmp;
    var len = arr.length;
    var ret = arr.slice();

    while (len) {
      rand = Math.floor(random(1) * len--);
      tmp = ret[len];
      ret[len] = ret[rand];
      ret[rand] = tmp;
    }

    console.log(ret);
    return ret;
  }

    function choosePalette(){
      var i = 0
      for (x in answers.genitals){
        i += Math.pow(2, x)
      }
      i += results.orientation
      return i;
  }
};
