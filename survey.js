{
 pages: [
  {
   elements: [
    {
     type: "checkbox",
     choices: [
      {
       value: "1",
       text: "penis-in-vagina"
      },
      {
       value: "2",
       text: "cunninglingus"
      },
      {
       value: "3",
       text: "etc"
      }
     ],
     name: "genitals",
     title: "Genital acts mean different things to different people.  Which acts do you do?"
    },
    {
     type: "rating",
     name: "boundaries",
     title: "What are the boundaries of your sexual experiences?",
     minRateDescription: "Limited to genital interaction",
     maxRateDescription: "Wide-ranging definition of sexual encounter."
    },
    {
     type: "rating",
     name: "relevant",
     title: "How relevant is your sexual identity to your overall identity?",
     minRateDescription: "Not at all",
     maxRateDescription: "Entirely dependent upon"
    },
    {
     type: "rating",
     name: "time",
     title: "How much time do you spend thinking about sex",
     minRateDescription: "Not at all",
     maxRateDescription: "All the time"
    },
    {
     type: "rating",
     name: "question1",
     title: "How much sex do you like to have",
     minRateDescription: "None",
     maxRateDescription: "Lots"
    },
    {
     type: "rating",
     name: "emotions",
     title: "How much emotional investment do you need to have sex with a person/people",
     minRateDescription: "None",
     maxRateDescription: "Lots"
    },
    {
     type: "rating",
     name: "role",
     title: "Do you subscribe to a particular role in the bedroom?",
     minRateDescription: "Same sitch every time",
     maxRateDescription: "I'm flex"
    },
    {
     type: "radiogroup",
     choices: [
      {
       value: "1",
       text: "Yes"
      },
      {
       value: "2",
       text: "No"
      }
     ],
     name: "aversion",
     title: "Do you avoid sex unless you are sure it will be good?"
    },
    {
     type: "rating",
     name: "autoerotic",
     title: "How important is self-pleasure to your sexual identity",
     minRateDescription: "Ain't no lover like myself",
     maxRateDescription: "Anyone but me"
    },
    {
     type: "radiogroup",
     choices: [
      {
       value: "1",
       text: "heterosexual"
      },
      {
       value: "2",
       text: "homosexual"
      },
      {
       value: "3",
       text: "bisexual"
      },
      {
       value: "4",
       text: "asexual"
      },
      {
       value: "5",
       text: "pansexual"
      },
      {
       value: "6",
       text: "demisexual"
      }
     ],
     name: "orientation",
     title: "What is your sexual orientation?"
    },
    {
     type: "panel",
     name: "panel1"
    }
   ],
   name: "page1"
  }
 ]
}